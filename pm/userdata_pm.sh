#!/bin/bash - 
declare puppet_repo_rpm="https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm"
declare host_name="puppet.vpc1.nasp"
declare host_ip="172.16.0.101"

#Update repo list with puppet repo via rpm
rpm -Uvh $puppet_repo_rpm
yum update -y

#Install Software 
yum install -y git vim ntp bind-utils puppetserver 

#Configure time and ntpd
timedatectl set-timezone America/Vancouver

#Use amazon ntp servers
sed -e 's/centos/amazon/g' -i /etc/ntp.conf 

#Update system time
ntpd -q

#Activate and enable ntpd
systemctl enable ntpd.service
systemctl start ntpd.service

#Configre Hostname and make it permanent
hostnamectl set-hostname $host_name
echo "preserve_hostname: true" >> /etc/cloud/cloud.cfg
#Change the fqdn of the host by changing the result of the local resolver
echo "$host_ip    $host_name" >> /etc/hosts


#Set Alias for puppet 
echo 'alias puppet="sudo /opt/puppetlabs/bin/puppet"' >> /home/centos/.bashrc

sed -e 's/2g/512m/g' -i /etc/sysconfig/puppetserver

touch /etc/puppetlabs/puppetserver/autosign.conf
echo "*.vpc1.nasp" >> /etc/puppetlabs/puppetserver/autosign.conf

