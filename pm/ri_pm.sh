#!/bin/bash - 
#Instance Specific Data
declare name_tag="pm"
declare ip="172.16.0.101" 
declare user_data="file://userdata_pm.sh"

#Type Specific Data
declare subnet="subnet-28650e5f"
declare security_groups="sg-6e53d10a"
declare acccess_key="mchen_aim_key"
declare instance_type="t2.micro"
declare ami="ami-d50a15e5"

#Create and Tag Instance
instance_id=$(aws ec2 run-instances --image-id $ami --instance-type $instance_type --key-name $acccess_key --security-group-ids $security_groups --subnet-id $subnet --private-ip-address $ip --user-data $user_data  --output text --query 'Instances[*].InstanceId')
aws ec2 create-tags --resource $instance_id --tags Key="Name",Value="$name_tag"
echo "EC2 Instance: $name_tag ID: $instance_id created"

