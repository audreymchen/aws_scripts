#!/bin/bash - 
declare puppet_repo_rpm="https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm"
declare host_name="www2.vpc1.nasp"
declare host_ip="172.16.1.102"

#Update repo list with puppet repo via rpm
rpm -Uvh $puppet_repo_rpm
yum update -y

#Install Software 
yum install -y git vim ntp puppet-agent nmap-ncat

#Configure time and ntpd
timedatectl set-timezone America/Vancouver

#Use amazon ntp servers
sed -e 's/centos/amazon/g' -i /etc/ntp.conf 

#Update system time
ntpd -q

#Activate and enable ntpd
systemctl enable ntpd.service
systemctl start ntpd.service

#Configre Hostname and make it permanent
hostnamectl set-hostname $host_name
echo "preserve_hostname: true" >> /etc/cloud/cloud.cfg
#Change the fqdn of the host by changing the result of the local resolver
echo "$host_ip    $host_name" >> /etc/hosts


#Set Alias for puppet 
echo 'alias puppet="sudo /opt/puppetlabs/bin/puppet"' >> /home/centos/.bashrc

#Need to reboot to make the changes to hosts /etc/hosts file effective
sudo reboot
