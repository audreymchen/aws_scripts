#!/bin/bash - 
#Instance Specific Data
declare name_tag="www1"
declare ip="172.16.1.101" 
declare public_ip_id="eipalloc-84b83ae1"
declare user_data="file://userdata_www1.sh"

#Type Specific Data
declare subnet="subnet-58650e2f"
declare security_groups="sg-a02eadc4"
declare acccess_key="mchen_aim_key"
declare instance_type="t2.micro"
declare ami="ami-d50a15e5" 

#Create and Tag Instance
instance_id=$(aws ec2 run-instances --image-id $ami --instance-type $instance_type --key-name $acccess_key --security-group-ids $security_groups --subnet-id $subnet --private-ip-address $ip --associate-public-ip-address --user-data $user_data  --output text --query 'Instances[*].InstanceId')
aws ec2 create-tags --resource $instance_id --tags Key="Name",Value="$name_tag"
echo "EC2 Instance: $name_tag ID: $instance_id created"


echo "Wating for instance to initialize"
while state=$(aws ec2 describe-instances --instance-ids $instance_id --output text --query 'Reservations[*].Instances[*].State.Name'); test "$state" = "pending"; do
    sleep 1; echo -n '.'
done; echo " $state"

#Associate with Elastic IP
aws ec2 associate-address --instance-id $instance_id --allocation-id $public_ip_id --output text
